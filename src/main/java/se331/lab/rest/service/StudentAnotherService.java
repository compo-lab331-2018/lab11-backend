package se331.lab.rest.service;

import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;

import java.util.List;

public interface StudentAnotherService {
    List<Student> getStudentByNameContains(String partofValue);
    List<Student> getStudentWhoseAdvisorNameIs(String advisorName);
}
