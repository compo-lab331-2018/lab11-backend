package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseAnotherDao;
import se331.lab.rest.entity.Course;
import se331.lab.rest.repository.CourseRepository;

import java.util.ArrayList;
import java.util.List;
@Service
public class CourseAnotherServiceImpl implements CourseAnotherService {
    @Autowired
    CourseAnotherDao courseAnotherDao;
    @Override
    public List<Course> getCourseWhichStudentEnrolledMoreThan(Integer amount) {
        List<Course> courses = courseAnotherDao.getAllCourse();
        List<Course> output = new ArrayList<>();
        for (Course course: courses) {
            int numEnrolledStudent = course.getStudents().size();
            if (numEnrolledStudent > amount) {
                output.add(course);
            }
        }
        return output;
    }
}
